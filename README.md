# just.sain wallpapers

![Screenshot of my desktop](https://gitlab.com/dwt1/dotfiles/raw/master/.screenshots/dotfiles10.png) 

Maybe it is just bordom wallpapers like me... But I like it!

## Where Did I Get These?

I find wallpapers in a number of different locations but good places to check out include [Imgur](http://imgur.com) and [/wg/](http://4chan.org/wg).  Some of the wallpapers were probably included in default wallpaper packages from various Linux distributions that I have installed over the years.

## Style of Wallpapers

The vast majority of these wallpapers are nature and landscape photos.	There are a few abstract art, anime and etc. wallpapers mixed in.
